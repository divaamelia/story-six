from django.test import TestCase, LiveServerTestCase
from django.urls import resolve

from .models import Status
from .views import index
from .forms import StatusForms

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class StorySixUnitTest(TestCase):

    def test_landing_page(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response,'Halo, apa kabar?' ,html = True)
    
    def test_using_correct_views(self):
        target = resolve('/')
        self.assertEqual(target.func, index)

    def test_models(self):
        new_object = Status.objects.create(status = "Hello, it's me")
        self.assertTrue(isinstance(new_object, Status))
        self.assertTrue(new_object.__str__(), new_object.status)
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def test_form(self):
        form_data = { 'status' : 'This is my new update.',}
        form = StatusForms(data = form_data)
        self.assertTrue(form.is_valid())

        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

class StorySixFunctionalTest(LiveServerTestCase):

    def setUp(self):
        # firefox
        super(StorySixFunctionalTest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()
        super(StorySixFunctionalTest, self).tearDown()

    def test_input_status(self):
        self.browser.get('http://127.0.0.1:8000')

        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('submit')

        status.send_keys("Coba Coba")
        submit.send_keys(Keys.RETURN)
