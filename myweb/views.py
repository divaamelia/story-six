from django.shortcuts import render,redirect
from .forms import StatusForms
from .models import Status

def index(request):
    if request.method == "POST":
        form = StatusForms(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.save()
            return redirect('index')
    else:
        form = StatusForms()
    status = Status.objects.all()
    return render (request, 'index.html', {'form' : form, 'status' : status})
